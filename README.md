# Kubernetes

## Install Kubectl (udemy)##

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl

chmod +x ./kubectl

sudo mv ./kubectl /usr/local/bin/kubectl

kubectl version
```

https://phoenixnap.com/kb/how-to-install-kubernetes-on-centos


## Install Minikube ##

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube

install minikube /usr/local/bin

minikube version

minikube start
```
---

# Docker

## Creating Docker Image (Ubuntu + Java) ##

**Dockerfile**

```
FROM ubuntu:18.04
 
MAINTAINER Naresh Chaurasia <message4naresh@gmail.com>
 
RUN apt-get update && apt-get install -y default-jdk
```


```docker build -t connect2tech/ubuntu_java . ```

```docker image ls```

```docker run -it connect2tech/ubuntu_java bash```

```docker push connect2tech/ubuntu_java```

---

## Creating Docker Image (Ubuntu + Java + Maven + Git + Jpetstore) ##

**Dockerfile**

```
FROM ubuntu:18.04

MAINTAINER Naresh Chaurasia <message4naresh@gmail.com>

RUN apt update

RUN apt install -y openjdk-8-jdk

RUN apt install -y maven

RUN apt install -y git

RUN git clone https://bitbucket.org/connect2tech/connect2tech.in-jpetstore

WORKDIR /connect2tech.in-jpetstore

ENTRYPOINT ["/usr/bin/mvn","jetty:run"]
```


```
docker build -t connect2tech/jpetstore .

docker run -d -p 8080:8080 connect2tech/jpetstore

http://192.168.56.101:8080/jpetstore

docker push connect2tech/jpetstore

```

---

## Docker Compose ##

### Setting up docker compose ###

```
curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

docker-compose --version
```
---

### Example1: Original Application ###
https://docs.docker.com/compose/gettingstarted/

```
docker-compose up

http://192.168.56.101:5000/
```

### Example1: Modified Application ###

 * build image connect2tech/python_web
 * Push to docker hub
 * Run from the image
 
```
working dir: /root/connect2nareshc/docker_compose/python_web

docker build -t connect2tech/python_web .

docker push connect2tech/python_web

docker-compose up
``` 

**docker-compose.yml**

```
version: '3'
services:
  web:
    image: "connect2tech/python_web"
    ports:
      - 5000:5000
  redis:
    image: "redis:alpine"
```
  
 

---

### Resources ###



---

## MySql ##

```
docker pull mysql/mysql-server:latest

docker run --name=my_sql -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 mysql/mysql-server:latest

docker exec -it my_sql mysql -uroot -p
```

---

## Docker Commands ##

```
docker run hello-world

docker run ubuntu sleep 5

#background/deteached mode
docker run -d ubuntu sleep 5

#interactive and terminal
docker run -it ubuntu bash
```

```
docker inspect mysql/mysql-server:latest
```

```
docker logs mysql/mysql-server:latest
```

```
docker ps --help

Usage:  docker ps [OPTIONS]

List containers

Options:
  -a, --all             Show all containers (default shows just running)
  -f, --filter filter   Filter output based on conditions provided
      --format string   Pretty-print containers using a Go template
      --help            Print usage
  -n, --last int        Show n last created containers (includes all states) (default -1)
  -l, --latest          Show the latest created container (includes all states)
      --no-trunc        Don't truncate output
  -q, --quiet           Only display numeric IDs
  -s, --size            Display total file sizes
```

```
docker stop --help

Usage:  docker stop [OPTIONS] CONTAINER [CONTAINER...]

Stop one or more running containers

Options:
      --help       Print usage
  -t, --time int   Seconds to wait for stop before killing it (default 10)
```

```
docker rm --help

Usage:  docker rm [OPTIONS] CONTAINER [CONTAINER...]

Remove one or more containers

Options:
  -f, --force     Force the removal of a running container (uses SIGKILL)
      --help      Print usage
  -l, --link      Remove the specified link
  -v, --volumes   Remove the volumes associated with the container
```

```
 docker images --help

Usage:  docker images [OPTIONS] [REPOSITORY[:TAG]]

List images

Options:
  -a, --all             Show all images (default hides intermediate images)
      --digests         Show digests
  -f, --filter filter   Filter output based on conditions provided
      --format string   Pretty-print images using a Go template
      --help            Print usage
      --no-trunc        Don't truncate output
  -q, --quiet           Only show numeric IDs
```

```
docker rmi --help

Usage:  docker rmi [OPTIONS] IMAGE [IMAGE...]

Remove one or more images

Options:
  -f, --force      Force removal of the image
      --help       Print usage
      --no-prune   Do not delete untagged parents
```

```
docker pull --help

Usage:  docker pull [OPTIONS] NAME[:TAG|@DIGEST]

Pull an image or a repository from a registry

Options:
  -a, --all-tags                Download all tagged images in the repository
      --disable-content-trust   Skip image verification (default true)
      --help                    Print usage
```

```
#Executing the command on running container
docker exec <running-container> cat /etc/hosts
```

```
#Running in detached mode.
docker run -d web-app

#Attaching back to container.
docker attach <docker-id/name>
```



---


## How to use Docker help !!!

```docker```

```docker images --help```

```docker ps --help```

```docker run --help```

---

## Important commands

```docker system info```

```docker images ls --digests```

```docker history redis```

## Use case 1

**Test Docker**

```docker run hello-world```


## Use case 2

```docker image pull redis```
